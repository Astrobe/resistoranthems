
mpd={}


mpd.songs = {
	-- We give the length as M+S +T, where M+T is the exact duration in minutes and T is the pause after the song
	-- for mild convenience. And just because we can.
	-- If you don't like a song, get its title and just remove the corresponding line below.
	-- Extra points if you remove the file in the sounds directory, that way your players' client
	-- won't download it for nothing.
	{name="01", length=60+54 +30, title="A Night Of Dizzy Spells"},
	{name="02", length=180+8 +30, title="Underclocked (underunderclocked mix)"},
	{name="03", length=120+3 +30, title="Chibi Ninja"},
	{name="04", length=120+57 +30, title="All of Us"},
	{name="05", length=180+19 +30, title="Come and Find Me"},
	{name="06", length=120+20 +30, title="Searching"},
	{name="07", length=120+21 +30, title="We're the Resistors"},
	{name="08", length= 180+12 +30, title="Ascending"},
	-- 09 was the original Underclocked (02).
	{name="10", length= 180+16 +30, title="Arpanauts"},
	-- 11 was "HHavok-intro". Too short (~1 minute)
	{name="12", length= 120+8 +30, title="HHavok-main"},
	{name="13", length= 180+16 +30, title="Digital Native"},
	{name="14", length= 120+2 +30, title="Jumpshot"},
	{name="15", length= 180+23 +30, title="Prologue"},
	{name="16", length= 240+32 +30, title="We're all under the stars"},
}

mpd.default_volume=1

mpd.storage = minetest.get_mod_storage()

mpd.handles={}

mpd.playing=true
mpd.song_time_left=0

minetest.register_globalstep(function(dtime)
	if mpd.playing then
		if mpd.song_time_left<=0 then
			mpd.stop_song()
			mpd.next_song()
		else
			mpd.song_time_left=mpd.song_time_left-dtime
		end
	end
end)
mpd.play_song=function(id)
	if mpd.playing then
		mpd.stop_song()
	end
	local song=mpd.songs[id]
	if not song then return end
	for _,player in ipairs(minetest.get_connected_players()) do
		local pname=player:get_player_name()
		local pvolume=tonumber(mpd.storage:get_string("vol_"..pname))
		pvolume=(pvolume or mpd.default_volume)/10
		if pvolume>0 then
			local handle = minetest.sound_play(song.name, {to_player=pname, gain=pvolume})
			mpd.handles[pname]=handle
		end
	end
	mpd.playing=id
	mpd.song_time_left = song.length
end
mpd.stop_song=function()
	for pname, handle in pairs(mpd.handles) do
		minetest.sound_stop(handle)
	end
	mpd.playing=nil
	mpd.handles={}
	mpd.time_next=nil
end

mpd.current=0

mpd.next_song=function()
	mpd.current=mpd.current+1

	if mpd.current == #mpd.songs then mpd.current=1 end

	-- Progressive shuffling. We randomly swap the next track with
        -- the next-next track.
	if mpd.current < #mpd.songs then
		if math.random()<0.5 then
			local temp=mpd.songs[mpd.current]
			mpd.songs[mpd.current]=mpd.songs[mpd.current+1]
			mpd.songs[mpd.current+1]=temp
		end
	end
	mpd.play_song(mpd.current)
end

minetest.register_chatcommand("music", {
	params = "[volume level (0-10)]",
	description = "Set your background music volume. Without parameters: show info.",
	privs = {},
	func = function(pname, param)
		if not param or param=="" then
			minetest.chat_send_player(pname, "Currently playing: "..mpd.songs[mpd.current].title)
			local pvolume=tonumber(mpd.storage:get_string("vol_"..pname)) or mpd.default_volume
			if pvolume>0 then
				return true, "Your music volume is set to "..pvolume.."."
			else
				if mpd.handles[pname] then
					minetest.sound_stop(mpd.handles[pname])
				end
				return true, "Background music is disabled for you. Use e.g. '/music 5' to enable it again."
			end
		end
		local pvolume=tonumber(param)
		if not pvolume then
			return false, "Invalid usage: /music [volume level (0-10)]"
		end
		pvolume = math.min(pvolume, 10)
		pvolume = math.max(pvolume, 0)
		mpd.storage:set_string("vol_"..pname, pvolume)
		if pvolume>0 then
			return true, "Music volume set to "..pvolume..". Change will take effect when the next song starts."
		else
			if mpd.handles[pname] then
				minetest.sound_stop(mpd.handles[pname])
			end
			return true, "Disabled background music for you. Use /music to enable it again."
		end
	end,		
})

