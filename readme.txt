This is a stripped-down version of MPD with a "chiptune" (8bit-like music)
playlist.

Players can control the volume with the /music chat command. Like the original MPD mod, it
can take some time before the music starts.

One day, I had the idea that 8bit music would fit Minetest's look-and-feel
pretty well, and should compress quite well (good for servers and download
times).  And indeed, we have here 30 minutes of music for less than 8mb.

Unfortunately, Players reported lags when a new player joins on my home test
server - but it only has a 1 Mb upstream connection.

Music: Eric Skiff - Resistor Anthems - Available at http://EricSkiff.com/music
Music: Eric Skiff - Resistor Anthems II - Available at http://EricSkiff.com/music

NOTE: the original MP3 have been converted to ogg format AND downgraded to
lower quality in order to downsize them nearly as much as possible.

Also, I was too lazy to convert the original filenames, so I kept the original
file number only. See the source for the conversion.

Author: Astrobe
Music license: Resistor Anthems I & II by Eric Skiff - CC-BY 4.0 (see license.txt)
Mod license: Derived from Mpd by Orwell - LGPL 2.1 (see LGL2-1.txt)

